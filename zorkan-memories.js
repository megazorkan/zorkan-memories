'use strict';

var express = require('express');
var app = express();

//credentials
var credentials = require('./credentials.js');

var router = express.Router();

//load models
var User = require('./models/user');

//set database connection
var mongoose = require('mongoose');
var opts = {
	server: {
		socketOptions: {
			keepAlive: 1
		}
	}
};
mongoose.connect(credentials.mongo.development.connectionString, opts);

//set up handlebars view engine
var handlebars = require('express3-handlebars')
	.create({
		defaultLayout: 'main'
	});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

//set cookie-parser
app.use(require('cookie-parser')(credentials.cookieSecret));

//set connect-flash
var flash = require('connect-flash');
app.use(flash());

//set port
app.set('port', process.env.PORT || 3000);

//set body parser
app.use(require('body-parser')());

//set static middleware
app.use(express.static(__dirname + '/public'));

//set expression
var expressSession = require('express-session');
app.use(expressSession({
	secret: credentials.session.secret
}));

//set passport
var passport = require('passport');

var localStrategy = require('./strategies/auth/local.js');
passport.use('local', localStrategy);

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		done(err, user);
	});
});

app.use(passport.initialize());
app.use(passport.session());

//importing routes
require('./routes.js')(app);

//custom 404 page
app.use(function(req, res) {
	res.render('main/404');
});

//custom 500 page
app.use(function(err, req, res, next) {
	console.log(err);
	res.render('main/500', {
		layout: 'main',
		error: err
	});
});

app.listen(app.get('port'), function() {
	console.log('Express started on http://localhost:' + app.get('port') + '; Ctrl+C to terminate.');
});