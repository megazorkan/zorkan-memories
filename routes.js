'use strict';

var main = require('./handlers/main.js');
var api = require('./handlers/api.js');
var admin = require('./handlers/admin.js');
var user = require('./handlers/user.js');
var auth = require('./handlers/auth.js');
module.exports = function(app) {

	//main
	app.get('/', main.home);
	app.get('/login', main.login);
	app.get('/signup', main.signup);
	app.post('/login', auth.loginLocal);
	app.post('/signup', main.checkSignup);

	//user
	app.get('/user/dashboard', requireAuthentication, user.dashboard);
	app.get('/user/memories', requireAuthentication, user.memories);
	app.get('/user/addmemory', requireAuthentication, user.addMemory);
	app.get('/user/memory', requireAuthentication, user.memory);
	app.get('/user/profile', requireAuthentication, user.profile);
	//api

	//admin
	app.get('/admin/users', admin.users);
	app.post('/admin/adduser', admin.addUser);
	app.get('admin/deleteuser/:user_id', admin.deleteUser);


	function requireAuthentication(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.redirect('/login');
		}
	}
};