'use strict';

var credentials = {
	cookieSecret: 'my cookie secret',

	mongo: {
		development: {
			connectionString: 'mongodb://zorkan:zorkan@ds035653.mongolab.com:35653/zorkan-memories'
		}
	},

	session: {
		secret: 'session secret'
	}
};

module.exports = credentials;