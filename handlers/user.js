'use strict';

var mongoose = require('mongoose');
var db = mongoose.connection;

require('../models/user');

exports.dashboard = function(req, res) {
	console.log('dashboard');
	res.render('user/dashboard', {
		layout: 'user'
	});
};

exports.addMemory = function(req, res) {
	res.render('user/addmemory', {
		layout: 'user'
	});
};

exports.memory = function(req, res) {

};

exports.memories = function(req, res) {
	res.render('user/memories', {
		layout: 'user'
	});
};

exports.profile = function(req, res) {
	console.log(req.user);
	res.render('user/profile', {
		layout: 'user'
	});
};