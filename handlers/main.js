'use strict';

var mongoose = require('mongoose');
var db = mongoose.connection;

var User = require('../models/user');

exports.home = function(req, res) {
	res.render('main/home');
};

exports.login = function(req, res) {
	res.render('main/login', {
		message: req.flash('message')
	});
};


exports.signup = function(req, res) {
	res.render('main/signup', {
		message: req.flash('message')
	});
};

exports.checkSignup = function(req, res) {
	var username = req.body.username,
		password = req.body.password,
		repeatPassword = req.body.repeatPassword,
		fullName = req.body.fullName;

	var user = new User({
		username: username,
		password: password,
		fullName: fullName
	});

	//fali jos provjera dali je username zauzet
	if (password === repeatPassword) {
		user.save(function(err) {
			if (err) {
				req.flash('message', 'Something went wrong while adding user.');
				res.redirect('/signup');
			} else {
				req.session.username = username;
				res.redirect('/user/dashboard');
			}
		});
	} else {
		req.flash('message', 'Make sure You entered valid username and/or passwords');
		res.redirect('/signup');
	}
};