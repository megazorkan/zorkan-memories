'use strict';

var passport = require('passport');
var mongoose = require('mongoose');
var db = mongoose.connection;

var User = require('../models/user');

exports.loginLocal = passport.authenticate('local', {
	successRedirect: '/user/dashboard',
	failureRedirect: '/login'
});