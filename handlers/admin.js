'use strict';

var mongoose = require('mongoose');
var db = mongoose.connection;

var User = require('../models/user');

//admin login, provjera u users u bazi na mongolabu
exports.login = function(req, res) {

};

exports.users = function(req, res) {

	if (req.session.flash) {
		res.locals.flash = req.session.flash;
		delete req.session.flash;
	}

	User.find({}, function(err, users) {
		if (users.length === 0) {
			res.locals.flash = {
				type: 'warning',
				intro: 'Empty database.',
				message: 'There are no users in database'
			};
		}

		res.render('admin/users', {
			users: users,
			layout: 'admin'
		});
	});

	/*
	db.collection('users').find().toArray(function(err, users) {
		if (users.length === 0) {
			res.locals.flash = {
				type: 'warning',
				intro: 'Empty database.',
				message: 'There are no users in database'
			};
		}

		res.render('admin/users', {
			users: users,
			layout: 'admin'
		});


	});
	*/
};

exports.addUser = function(req, res) {

	var username = req.body.username,
		password = req.body.password,
		fullName = req.body.fullName;


	var user = new User({
		username: username,
		password: password,
		fullName: fullName
	});
	user.save(function(err) {
		if (err) {
			req.session.flash = {
				type: 'danger',
				intro: 'Insertion error!',
				message: 'Something went wrong while adding user.',
			};
			res.redirect('/admin/users');
		} else {
			req.session.flash = {
				type: 'success',
				intro: 'Success!',
				message: 'User successfully added.',
			};
			res.redirect('/admin/users');
		}
	});
	/*
	db.collection('users').insert({
		username: username,
		password: password,
		fullName: fullName,
		memberSince: new Date()
	}, function(error, success) {
		if (error) {
			req.session.flash = {
				type: 'danger',
				intro: 'Insertion error!',
				message: 'Something went wrong while adding user.',
			};
			res.redirect('/admin/users');
		} else {
			req.session.flash = {
				type: 'success',
				intro: 'Success!',
				message: 'User successfully added.',
			};
			res.redirect('/admin/users');

		}
	});
	*/
};

exports.deleteUser = function(req, res) {
	console.log('jel ovo krenulo');
	console.log(req);
	res.redirect('/admin/users');
};