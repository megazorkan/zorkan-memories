'use strict';

var mongoose = require('mongoose');

var memorySchema = mongoose.Schema({
	userId: String,
	cityName: String,
	startDate: Date,
	endDate: Date,
	review: String
});

var Memory = mongoose.model('Memory', memorySchema);
module.exports = Memory;