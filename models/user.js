'use strict';

var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	username: String,
	password: String,
	fullName: String,
	memberSince: Number
});

userSchema.methods.getMemberSinceInDays = function() {
	return this.memberSince;
};

userSchema.methods.validPassword = function(password) {
	if (this.password === password) {
		return true;
	} else {
		return false;
	}
};

var User = mongoose.model('User', userSchema);
module.exports = User;